var EventEmitter = require('events');

var debug = require('debug')('bot-client');

require('./helpers');

var Socket = require('./Classes/Socket');
var ObjectCollection = require('./Classes/ObjectCollection');

const Constants = require('./Constants');

/** The isleward-api Client */
class Client extends EventEmitter {
  /**
   * Create the client
   * @param {string} url - The isleward server url to connect to
   */
  constructor(url) {
    super();
    this.Dispatcher = new EventEmitter();
    this.Socket = new Socket(this, url);

    this.Objects = new ObjectCollection(this);

    debug('New client created');
  }

  /**
   * Login with username & password, should be done after the READY event
   * @param {string} username - Account username
   * @param {string} password - Account password
   * @param {Client~login} cb
   */
  login(username, password, cb) {
    var data = {};
    if (typeof username === 'object') {
      cb = password;
      data = username;
    }
    return this.Socket.login(data, cb);
  }
  /**
   * Login callback
   * @callback Client~login
   * @param {String|null} error
   */

  /**
   *  Get the list of characters on the account
   * @param {Client~getCharacterList} cb
   */
  getCharacterList(cb) {
    this.Socket.request({
      cpn: 'auth',
      method: 'getCharacterList'
    }, cb);
  }
  /**
   * getCharacterList callback
   * @callback Client~getCharacterList
   * @param {Object[]} characters
   * @param {String} characters[].name - Name of the character
   * @param {Number} characters[].level - Level of the character
   */

  /**
   * Gets information about a character
   * @param {String} name - Name of the character
   * @param {Client~getCharacter} - cb
   */
  getCharacter(name, cb) {
    this.Socket.request({
      cpn: 'auth',
      method: 'getCharacter',
      data: { name: name }
    }, cb);
  }
  /**
   * @callback Client~getCharacter
   * @param {Object[]} components - A whole bunch of component stuff
   * @todo Write component documentation?
   * @param {Number} id - Character id, i think?
   * @param {String} sheetName
   * @param {String} layerName
   * @param {Number} cell
   * @param {String} name - Character's name
   * @param {String} class - Character's class
   * @param {String} zoneName - Current zone
   * @param {Number} x - X position
   * @param {Number} y - Y position
   * @param {String} account - User account name
   * @param {Number} xp - Character's xp
   * @param {Number} level - Character's level
   * @param {Number} zone
   * @param {Number} serverId
   * @param {Number} zoneId
   */

  /**
   * Get skin sheet positions
   * @param {Client~getSkins} cb
   */
  getSkins(cb) {
    this.Socket.request({
      cpn: 'auth',
      method: 'getSkins'
    }, cb);
  }
  /**
   * @callback Client~getSkins
   * @param {Object} skins
   */

  /**
   * Create a character
   * @param {String} name
   * @param {String} class
   * @param {Number} costume
   * @param {String[]} prophecies
   * @param {Client~createCharacter} cb
   */
  createCharacter(name, _class, costume, prophecies, cb) {
    this.Socket.request({
      cpn: 'auth',
      method: 'createCharacter',
      data: {
        name: name,
        class: _class,
        costume: costume,
        prophecies: prophecies
      }
    }, cb);
  }
  /**
   * @callback Client~createCharacter
   * @param {null}
   */

  play(charname, cb) {
    this.Socket.request({
      cpn: 'auth',
      method: 'play',
      data: { name: charname }
    }, cb);
  }
}

Client.Constants = Constants;

module.exports = Client;
