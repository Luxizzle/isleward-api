var io = require('socket.io-client')

const Constants = require('./../Constants')

var debug = require('debug')('bot-socket')

class Socket { 
  constructor(self, url) {
    this.self = self;

    this.socket = io(url, {
      transports: [ 'websocket' ]
    })

    this.socket.on('connect', this.onConnect.bind(this))
    this.socket.on('handshake', this.onHandshake.bind(this))
    this.socket.on('dc', this.onDisconnect.bind(this))

    this.socket.on('event', this.onEvent.bind(this))
    this.socket.on('events', this.onEvents.bind(this))
  }

  login(data, cb) { 
    return this.request({
      cpn: 'auth',
      method: 'login',
      data: data
    }, (err) => {
      if (err) throw new Error(err);
      this.self.emit(Constants.EVENTS.LOGIN)
      if (cb) return cb();
    }) 
  }

  request(data, cb) { return this.socket.emit('request', data, cb) }

  onConnect() { 
    debug('onConnect')
    this.self.emit(Constants.EVENTS.CONNECT)
  }

  onHandshake() {
    debug('onHandshake')
    this.socket.emit('handshake') // send handshake back, bff
    this.self.emit('READY') // send this to tell the user its safe to log in
  }

  onDisconnect() { 
    debug('onDisconnect')
    this.self.emit(Constants.EVENTS.DISCONNECT)
  }

  onEvent(res) {
    this.self.Dispatcher.emit(res.event, res.data)
  }

  onEvents(res) {
    var oList = res.onGetObject;
    if (oList) {
      var prepend = oList.filter(function(o) {
        return o.self;
      });
      oList.spliceWhere(function(o) {
        return prepend.some(function(p) { return p == o; });
      });
      oList.unshift.apply(oList, prepend);
    }

    for (var e in res) {
      var r = res[e];
      r.forEach((o) => {
        this.self.Dispatcher.emit(e, o)
      });
    }
  }
}

module.exports = Socket