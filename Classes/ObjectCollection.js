var debug = require('debug')('bot-object');
var _ = require('lodash');

class ObjectCollection extends Array {
  constructor(self) {
    super();
    this.self = self;

    this.self.Dispatcher.on('onGetObject', this.onGetObject.bind(this));
  }

  getBy(key, value) {
    if (!value) {
      value = key;
      key = 'id';
    }
    return this.find((d) => d[key] === value);
  }

  onGetObject(data) {
    //debug(data);
    var id = data.id;
    if (!id) return; // Attacks dont have ids, figure that out.
    var obj = this.getBy(id);
    if (obj === undefined) {
      this.createObject(data);
    } else {
      this.updateObject(obj, data);
    }
  }

  createObject(data) {
    this.push(data);
    //debug(data);
  }

  updateObject(obj, data) {
    obj = _.extend(obj, data);
    //debug(obj);
  }
}

module.exports = ObjectCollection;

