var data = {
  EVENTS: {
    READY: 'READY',
    LOGIN: 'LOGIN',
    PLAY: 'PLAY'
  }
}

module.exports = data;