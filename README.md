# Isleward-api

A simple inteface to interact with the isleward api

More or less a project to reverse engineer the websocket api

I will not add a way to register new accounts, you can figure this out by yourself, but i highly discourage it for obvious reasons.

## Usage

Install module with `npm i gitlab:Luxizzle/isleward-api` (I will probably never upload this to npm ¯\\_(ツ)_/¯)

Documentation over at [`DOCS.md`](/DOCS.md)