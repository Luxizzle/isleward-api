<a name="Client"></a>

## Client
The isleward-api Client

**Kind**: global class  

* [Client](#Client)
    * [new Client(url)](#new_Client_new)
    * _instance_
        * [.login(username, password, cb)](#Client+login)
        * [.getCharacter(name, cb)](#Client+getCharacter)
        * [.getSkins(cb)](#Client+getSkins)
        * [.createCharacter(name, class, costume, prophecies, cb)](#Client+createCharacter)
    * _inner_
        * [~getCharacterList](#Client..getCharacterList) : <code>function</code>
        * [~login](#Client..login) : <code>function</code>
        * [~getCharacter](#Client..getCharacter) : <code>function</code>
        * [~getSkins](#Client..getSkins) : <code>function</code>
        * [~createCharacter](#Client..createCharacter) : <code>function</code>

<a name="new_Client_new"></a>

### new Client(url)
Create the client


| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> | The isleward server url to connect to |

<a name="Client+login"></a>

### client.login(username, password, cb)
Login with username & password, should be done after the READY event

**Kind**: instance method of <code>[Client](#Client)</code>  

| Param | Type | Description |
| --- | --- | --- |
| username | <code>string</code> | Account username |
| password | <code>string</code> | Account password |
| cb | <code>[login](#Client..login)</code> |  |

<a name="Client+getCharacter"></a>

### client.getCharacter(name, cb)
Gets information about a character

**Kind**: instance method of <code>[Client](#Client)</code>  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>String</code> | Name of the character |
| cb | <code>[getCharacter](#Client..getCharacter)</code> | cb |

<a name="Client+getSkins"></a>

### client.getSkins(cb)
Get skin sheet positions

**Kind**: instance method of <code>[Client](#Client)</code>  

| Param | Type |
| --- | --- |
| cb | <code>[getSkins](#Client..getSkins)</code> | 

<a name="Client+createCharacter"></a>

### client.createCharacter(name, class, costume, prophecies, cb)
Create a character

**Kind**: instance method of <code>[Client](#Client)</code>  

| Param | Type |
| --- | --- |
| name | <code>String</code> | 
| class | <code>String</code> | 
| costume | <code>Number</code> | 
| prophecies | <code>Array.&lt;String&gt;</code> | 
| cb | <code>[createCharacter](#Client..createCharacter)</code> | 

<a name="Client..getCharacterList"></a>

### Client~getCharacterList : <code>function</code>
Get the list of characters on the account

**Kind**: inner typedef of <code>[Client](#Client)</code>  

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>[getCharacterList](#Client..getCharacterList)</code> | getCharacterList callback |
| characters | <code>Array.&lt;Object&gt;</code> |  |
| characters[].name | <code>String</code> | Name of the character |
| characters[].level | <code>Number</code> | Level of the character |

<a name="Client..login"></a>

### Client~login : <code>function</code>
Login callback

**Kind**: inner typedef of <code>[Client](#Client)</code>  

| Param | Type |
| --- | --- |
| error | <code>String</code> &#124; <code>null</code> | 

<a name="Client..getCharacter"></a>

### Client~getCharacter : <code>function</code>
**Kind**: inner typedef of <code>[Client](#Client)</code>  
**Todo**

- [ ] Write component documentation?


| Param | Type | Description |
| --- | --- | --- |
| components | <code>Array.&lt;Object&gt;</code> | A whole bunch of component stuff |
| id | <code>Number</code> | Character id, i think? |
| sheetName | <code>String</code> |  |
| layerName | <code>String</code> |  |
| cell | <code>Number</code> |  |
| name | <code>String</code> | Character's name |
| class | <code>String</code> | Character's class |
| zoneName | <code>String</code> | Current zone |
| x | <code>Number</code> | X position |
| y | <code>Number</code> | Y position |
| account | <code>String</code> | User account name |
| xp | <code>Number</code> | Character's xp |
| level | <code>Number</code> | Character's level |
| zone | <code>Number</code> |  |
| serverId | <code>Number</code> |  |
| zoneId | <code>Number</code> |  |

<a name="Client..getSkins"></a>

### Client~getSkins : <code>function</code>
**Kind**: inner typedef of <code>[Client](#Client)</code>  

| Param | Type |
| --- | --- |
| skins | <code>Object</code> | 

<a name="Client..createCharacter"></a>

### Client~createCharacter : <code>function</code>
**Kind**: inner typedef of <code>[Client](#Client)</code>  

| Type |
| --- |
| <code>null</code> | 

