var Client = require('../index.js');
var config = require('./config.json');

var debug = require('debug')('bot');

var bot = new Client(config.url);

bot.on('READY', function() {
  debug('READY');

  bot.login(config.auth, function(err) {
    if (err) throw new Error(err);
    debug('Logged in');
/*
    bot.getCharacterList(function(chars) {
      debug(chars);
    });

    bot.getCharacter(config.charname, (char) => {
      debug(char);
    });
*/
    bot.getCharacter(config.charname, (char) => {
      debug('name: '+char.name+', level: '+char.level);
      bot.play(char.name, () => {
        debug('play', arguments);
      });
    });
/*
    bot.createCharacter('test', 'warrior', 0, [], function(result) {
      debug(result);
    });
*/
  });
});

